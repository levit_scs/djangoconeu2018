from django.contrib.auth import get_user_model

from rest_framework.viewsets import ModelViewSet
from django_filters.rest_framework import DjangoFilterBackend

from .serializers import UserSerializer


class UserViewSet(ModelViewSet):

    serializer_class = UserSerializer
    queryset = get_user_model().objects.all()
    filter_backends = (DjangoFilterBackend, )
    filter_fields = ('first_name', 'last_name',)

