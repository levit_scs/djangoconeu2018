from rest_framework import viewsets

from drf_auto_endpoint.endpoints import Endpoint
from drf_auto_endpoint.router import register
from drf_auto_endpoint.factories import serializer_factory

from .models import Category, Product
from .serializers import ProductSerializer


class ProductViewSet(viewsets.ReadOnlyModelViewSet):

    def retrieve(self, request, *args, **kwargs):
        obj = self.get_object()
        obj.views += 1
        obj.save()
        return super(ProductViewSet, self).retrieve(request, *args, **kwargs)

    def get_serializer_class(self):
        if getattr(self, 'action', None) == 'retrieve':
            return serializer_factory(self.endpoint, base_class=ProductSerializer)
        return super(ProductViewSet, self).get_serializer_class()
