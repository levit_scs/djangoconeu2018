from django.db.models import F
from django.shortcuts import get_object_or_404

from drf_auto_endpoint.decorators import bulk_action, custom_action, wizard
from drf_auto_endpoint.endpoints import Endpoint
from drf_auto_endpoint.router import register

from rest_framework.response import Response

from .models import Category, Product
from .serializers import ProductSerializer, LikeCountSerializer
from .views import ProductViewSet

@register
class ProductEndpoint(Endpoint):

    model = Product
    read_only = True
    search_fields = ('name', )
    filter_fields = ('category_id', )
    ordering_fields = ('price', 'name', )
    base_viewset = ProductViewSet
    # base_serializer = ProductSerializer

    @custom_action(method='POST')
    def like(self, request, pk):
        obj = get_object_or_404(self.model, pk=pk)
        obj.likes += 1
        obj.save()
        return Response(self.get_serializer(obj).data)

    @wizard(LikeCountSerializer)
    def dislike(self, request, pk):
       obj = get_object_or_404(self.model, pk=pk)
       obj.likes -= request.validated_data['amount']
       obj.save()
       return Response(self.get_serializer(obj).data)

    @bulk_action(method='POST')
    def reset_likes(self, request):
        self.model.objects.all().update(likes=0)
        return Response(status=204)

    @wizard(LikeCountSerializer, meta_type='list')
    def cheatlikes(self, request):
        self.model.objects.all().update(
            likes=F('likes') + request.validated_data['amount']
        )
        return Response(status=204)


@register
class CategoryEndpoint(Endpoint):

    model = Category
    search_fields = ('name', )
    read_only = True
