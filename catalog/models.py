from django.db import models

class Category(models.Model):

    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Product(models.Model):

    name = models.CharField(max_length=255)
    category = models.ForeignKey(Category, related_name='products', on_delete=models.CASCADE)
    description = models.TextField()
    image = models.URLField()
    price = models.DecimalField(max_digits=6, decimal_places=2)
    likes = models.SmallIntegerField(default=0)
    views = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.name
