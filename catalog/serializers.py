from drf_auto_endpoint.factories import serializer_factory
from rest_framework import serializers

from .models import Category


class ProductSerializer(serializers.ModelSerializer):

    category = serializer_factory(model=Category, fields=('id', 'name'))()


class LikeCountSerializer(serializers.Serializer):
    amount = serializers.IntegerField(min_value=1)

    class Meta:
        fields = ('amount', )
